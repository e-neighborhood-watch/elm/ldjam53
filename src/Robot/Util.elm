module Robot.Util
  exposing
    ( disconnected
    )


import Robot.Type
  as Robot
import Util.Direction.Type
  as Direction
import Util.Map.Empty
  as Map
import Util.Map.Member
  as Map
import Util.Map.Remove
  as Map
import Util.Map.Type
  exposing
    ( Map
    )
import Util.Maybe.ToList
  as Maybe
import Util.Position.Move
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


disconnected : Robot.Robot -> Map Int Robot.Piece
disconnected =
  let
    go : List (Position Int) -> Map Int Robot.Piece -> Map Int Robot.Piece
    go stack pieceMap =
      case
        stack
      of
        [] ->
          pieceMap
        head :: tail ->
          let
            heads =
              [ Direction.North
              , Direction.East
              , Direction.South
              , Direction.West
              ]
              |> List.map
                (Position.moveBy head)
              |> List.filter
                (Map.memberOf pieceMap)
          in
            go (heads ++ tail) (List.foldl Map.remove pieceMap heads)
  in
    (.pieceMap) >> go [Position 0 0]
