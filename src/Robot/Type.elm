module Robot.Type
  exposing
    ( Robot
    , Component (..)
    , pieces
    , pieceId
    , weight
    , TreadDirection (..)
    )

import Dict
  exposing
    ( size
    )

import Util.Map.Type
  exposing
    ( Map
    )
import Util.Direction.Type
  exposing
    ( Direction
    )

type TreadDirection
  = Vertical
  | Horizontal
  | Omni

type Component
  = Glome
  | Treads TreadDirection Int
  | Shield
  | Drill Direction Bool
  | Cargo -- cargo should also take a Package as a parameter, but a Package can be a Component, and that gets recursive
  | Sensor Int
  | Memory Int
  | Scanner Int
  | StasisField

pieces : List Component
pieces =
  [ Glome
  ]


pieceId : Component -> Int
pieceId piece =
  case
    piece
  of
    Glome ->
      0
    _ ->
      -1

-- Assume core is at (0,0)
type alias Robot =
  { pieceMap :
    Map Int Component
  }

-- In the future, this could allow for variable-weight compoments
weight : Robot -> Int
weight robot =
  Dict.size robot.pieceMap

