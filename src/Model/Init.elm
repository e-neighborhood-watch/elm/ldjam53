module Model.Init
  exposing
    ( init
    )


import Data.Levels.Level1 as Level1


import Level.Status.Type
  as Status
import LevelSelect.Init
  as LevelSelect
import Model.Type
  as Model
  exposing
    ( Model
    )
import Message.Type
  exposing
    ( Message
    )
import Settings.Init
  as Settings


init : a -> ( Model, Cmd Message )
init _ =
  ( { settings =
      Settings.init
    , levels =
      LevelSelect.init
        Level1.level
        [
        ]
    , model =
      Model.LevelSelect
    }
  , Cmd.none
  )

