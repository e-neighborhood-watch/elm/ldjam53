module Data.Text.Type
  exposing
    ( Text (..)
    )


type Text
  = HelloWorld
  | LevelSelectTitle
  | SettingsTitle
  | Level1Title
