module Data.Levels.Level1
  exposing
    ( level
    )


import Dict
  exposing
    ( Dict
    )


import Data.Text.Type
  as Text
import Level.Type
  exposing
    ( Level
    )


level : Level
level =
  { tileMap =
    Dict.empty
  , bound =
    { x =
      6
    , y =
      20
    }
  , coreLocation =
    { dx =
      2
    , dy =
      4
    }
  , title =
    Text.Level1Title
  , description =
    Nothing
  , robot =
    { pieceMap =
      Dict.empty
    }
  }
