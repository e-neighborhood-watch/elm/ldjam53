module RobotCreator.Update
  exposing
    ( adjust
    )


import Dict
  as Dict
  exposing
    ( Dict
    )


import Robot.Type
  as Robot
  exposing
    ( Robot
    )
import RobotCreator.Model.Type
  as Model
  exposing
    ( Model
    )
import Util.Map.Get
  exposing
    ( get
    )
import Util.Map.Insert
  as Map
import Util.Position.Type
  exposing
    ( Position
    )


-- TODO implement this function for real
mousePosToGridPos : Position Float -> Position Int
mousePosToGridPos _ =
  Position 9 5


-- this function should get put somewhere more central
adjust : comparable -> Int -> Dict comparable Int -> Dict comparable Int
adjust key delta =
  Dict.update key (Maybe.withDefault 0 >> (+) delta >> Just)


placePiece : Robot.Piece -> Position Int -> Robot -> Robot
placePiece piece pos robot =
  { robot
  | pieceMap =
    Map.insert pos piece robot.pieceMap
  }


releaseHeld : Model -> Model
releaseHeld { robot, availablePieces, held } =
  case
    held
  of
    Nothing ->
      { robot =
        robot
      , availablePieces =
        availablePieces
      , held =
        held
      }
    Just { pos, target } ->
      case
        target
      of
        Model.Piece piece ->
          let
            gridPos =
              mousePosToGridPos pos
          in
            case
              get gridPos robot.pieceMap
            of
              Nothing ->
                { robot =
                  placePiece piece gridPos robot
                , availablePieces =
                  availablePieces
                , held =
                  Nothing
                }
              Just existingPiece ->
                { robot =
                  placePiece piece gridPos robot
                , availablePieces =
                  adjust (Robot.pieceId existingPiece) 1 availablePieces
                , held =
                  Nothing
                }
        _ ->
          { robot =
            robot
          , availablePieces =
            availablePieces
          , held =
            held
          }
