module RobotCreator.Model.Type
  exposing
    ( Model
    , MouseTarget (..)
    )


import Dict
  exposing
    ( Dict
    )


import Robot.Type
  as Robot
  exposing
    ( Robot
    )
import Util.Position.Type
  exposing
    ( Position
    )


type MouseTarget
  = Background
  | Core
  | Piece Robot.Piece


type alias Model =
  { robot :
    Robot
  , availablePieces :
    Dict Int Int
  , held :
    Maybe
      { pos :
        Position Float
      , target :
        MouseTarget
      }
  }
