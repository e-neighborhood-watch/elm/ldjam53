module Animation.Movement.Type
  exposing
    ( Movement
    )


import Animation.Animated.Type
  exposing
    ( Animated
    )
import Util.Offset.Type
  exposing
    ( Offset
    )


type alias Movement =
  Animated (Offset Float)
