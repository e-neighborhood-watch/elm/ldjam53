module Level.Package.Type
  exposing
    ( Package (..)
    )

import Level.Robot.Component
  as Component

type Package
  = ComponentPackage Component
  | DeliveryPackage

