module Level.Type
  exposing
    ( Level
    , get
    )


import Data.Text.Type
  exposing
    ( Text
    )
import Level.Item.Type
  exposing
    ( Item
    )
import Level.Tile.Type
  exposing
    ( Tile
    )
import Util.Map.Type
  exposing
    ( Map
    )
import Util.Offset.Type
  exposing
    ( Offset
    )
import Util.Position.Type
  exposing
    ( Position
    )
import Robot.Type
  exposing
    ( Robot
    )

type alias Level =
  { tileMap :
    Map Int Tile
  , bound :
    Position Int
  , coreLocation :
    Offset Int
  , robot :
    Robot
  , title :
    Text
  , description :
    Maybe Text
  }


get :
  { a
  | tileMap :
    Map Int Tile
  , bound :
    Position Int
  , coreLocation :
    Offset Int
  , robot:
    Robot
  , title :
    Text
  , description :
    Maybe Text
  }
    -> Level
get x =
  { tileMap =
    x.tileMap
  , bound =
    x.bound
  , coreLocation =
    x.coreLocation
  , robot =
    x.robot
  , title =
    x.title
  , description =
    x.description
  }
