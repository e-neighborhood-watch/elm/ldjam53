module Level.Message.Type
  exposing
    ( Message (..)
    )


import Util.Direction.Type
  exposing
    ( Direction
    )


type Message
  = Noop
  | Exit
  | TimePasses Int
  | Move Direction
  | AddPlayerAnimation Direction Int
