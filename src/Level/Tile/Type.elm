module Level.Tile.Type
  exposing
    ( Tile (..)
    )


type Tile
  = Rock
  | HardRock
  | Pit
