module Level.View
  exposing
    ( view
    )


import Css
import Svg.Styled
  as Svg
import Svg.Styled.Attributes
  as SvgAttr


import Animation.Movement.Offset
  as Movement
import Data.Color.Theme.Type
  as Color
import Data.Font.Size
  as Font
import Document.Type
  exposing
    ( Document
    )
import Level.Message.Type
  exposing
    ( Message
    )
import Level.Model.Type
  exposing
    ( Model
    )
import Settings.Color.Theme
  as Theme
import Settings.Color.Theme.Type
  as Theme
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Language
  as Language
import Settings.Type
  as Settings
  exposing
    ( Settings
    )
import Util.Offset.Type
  exposing
    ( Offset
    )


view : Settings -> Model -> Document Message
view settings { level, playerAnimation } =
  let
    playerTransform : String
    playerTransform =
      case
        playerAnimation
      of
        Nothing ->
          ""

        Just animation ->
          let
            animationOffset : Offset Float
            animationOffset =
              Movement.offset animation
          in
            "translate("
              ++ String.fromFloat animationOffset.dx
              ++ " "
              ++ String.fromFloat animationOffset.dy
              ++ ")"
    viewBox =
      { min =
        { x =
          -0.6
        , y =
          -0.5 - Font.rawSize Font.Title settings.fontSize
        }
      , max =
        { x =
          toFloat level.bound.x + 1.2
        , y =
          toFloat level.bound.y + 2.5
        }
      }
    rectBox =
      { min =
        { x =
          -0.3
        , y =
          -0.3
        }
      , max =
        { x =
          toFloat level.bound.x + 0.3
        , y =
          toFloat level.bound.y + 0.3
        }
      }
  in
    { title =
      Language.fromText settings.language level.title
    , viewBox =
      viewBox
    , body =
      [ Svg.text_
        [ Font.toCss Font.Title settings.fontSize
        , SvgAttr.y "-0.45"
        , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
        ]
        [ Svg.text (Language.fromText settings.language level.title)
        ]
      , Svg.rect
        [ SvgAttr.x (rectBox.min.x |> String.fromFloat)
        , SvgAttr.y (rectBox.min.y |> String.fromFloat)
        , SvgAttr.fill (Theme.toHex settings.theme Color.Background)
        , SvgAttr.width (rectBox.max.x - rectBox.min.x |> String.fromFloat)
        , SvgAttr.height (rectBox.max.y - rectBox.min.y |> String.fromFloat)
        , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
        , SvgAttr.strokeWidth "0.1"
        ]
        []
      , Svg.use
        [ SvgAttr.xlinkHref "#Character"
        , level.coreLocation.dx
          |> String.fromInt
          |> SvgAttr.x
        , level.coreLocation.dy
          |> String.fromInt
          |> SvgAttr.y
        , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
        , SvgAttr.transform playerTransform
        ]
        [
        ]
      ]
    }
