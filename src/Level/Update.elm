module Level.Update
  exposing
    ( update
    )


import Task
import Time
import Dict
  exposing
    ( toList
    )

import Animation.Animated.Update
  as Animated
import Level.Type
  exposing
    ( Level
    )
import Level.Message.Type
  as Message
  exposing
    ( Message
    )
import Level.Model.Type
  exposing
    ( Model
    )
import Level.Tile.Type
  exposing
    ( Tile (..)
    )
import Level.Update.Result.Type
  as Update
import Robot.Type
  exposing
    ( Robot
    , Component (..)
    , weight
    , TreadDirection (..)
    )
  
import Util.Box.Within
  as Box
import Util.Direction.Type
  exposing
    ( Direction (..)
    )
import Util.Offset.Add
  as Offset
import Util.Offset.FromDirection
  as Offset
import Util.Offset.Invert
  as Offset
import Util.Offset.Scale
  as Offset
import Util.Offset.ToFloat
  as Offset
import Util.Offset.Type
  exposing
    ( Offset
    )
import Util.Position.Move
  as Position
import Util.Position.Translate
  as Position
import Util.Position.Type
  exposing
    ( Position
    )
import Util.Map.Map
  as Map
import Util.Map.Merge
  as Map
import Util.Map.Remove
  as Map

treadRelevant : TreadDirection -> Direction -> Bool
treadRelevant td dir =
  case
    td
  of
    Omni ->
      True
    Vertical ->
      (dir == North) || (dir == South)
    Horizontal ->
      (dir == East) || (dir == West)

treadPower : Direction -> Level -> Int
treadPower dir level =
  -- i could do this with Dict.foldl but that sounds more confusing
  List.map
    (\ (pos, piece) ->
      case
        piece
      of
        Treads treadDir power ->
          if
            (treadRelevant treadDir dir)
          then
            -- TODO in the future, check level.tileMap for pits here
            power
          else
            0
        _ ->
          0
    )
    ( Dict.toList level.robot.pieceMap )
  |> List.sum
  

collide : Direction -> Position Int -> Tile -> Component -> Maybe Level -> Maybe Level
collide dir pos tile piece maybeLevel =
  case
    maybeLevel
  of
    Nothing ->
      Nothing
    Just level ->
      let
        robot = level.robot
        posOnRobot = Position.translate (Offset.invert level.coreLocation) pos
      in
        case
          tile
        of
          Rock ->
            case
              piece
            of
              Shield ->
                Nothing
              Drill drillDir True ->
                if
                  drillDir == dir
                then
                  Just
                    { level
                    | tileMap =
                      Map.remove pos level.tileMap
                    , robot =
                      robot
                    }
                else
                  Just
                    { level
                    | tileMap =
                      Map.remove pos level.tileMap
                    , robot =
                      { robot
                      | pieceMap =
                        Map.remove posOnRobot level.robot.pieceMap
                      }
                    }
              StasisField ->
                Just level
              _ ->
                Just
                  { level
                  | tileMap =
                    Map.remove pos level.tileMap
                  , robot =
                    { robot
                    | pieceMap =
                      Map.remove posOnRobot level.robot.pieceMap
                    }
                  }
          HardRock ->
            case
              piece
            of
              Shield ->
                Nothing
              StasisField ->
                Just level
              _ ->
                Just
                  { level
                  | robot =
                    { robot
                    | pieceMap =
                      Map.remove posOnRobot level.robot.pieceMap
                    }
                  }
          _ ->
            Just level

moveRobot : Direction -> Model -> Model
moveRobot dir model =
  let
    level =
      model.level
  in
    if
      treadPower dir level >= weight level.robot
    then
      -- TODO this doesn't account for packages on the ground
      case
        Map.merge
          ( \ _ _ r -> r )
          ( collide dir )
          ( \ _ _ r -> r )
          level.tileMap
          ( Map.keyMap
            ( Position.move dir >> Position.translate level.coreLocation)
            level.robot.pieceMap
          )
          ( Just
            { level
            | coreLocation =
              Offset.add ( Offset.fromDirection dir ) level.coreLocation
            }
          )
      of
        Nothing ->
          model
        Just newLevel ->
          { model
          | level =
            newLevel
          }
    else
      model

--so basically uh. for all the robot parts, look at what they would hit
--if there's a rock, destroy the rock and the part
--if there's a hard rock, destroy the part
    

update : Message -> Model -> Update.Result
update msg { level, playerAnimation } =
  case
    msg
  of
    Message.Noop ->
      Update.NoChange

    Message.Exit ->
      Update.Exit

    Message.TimePasses newTime ->
      case
        playerAnimation
      of
        Nothing ->
          Update.NoChange

        Just animation ->
          Update.InternalChange
            { level =
              level
            , playerAnimation =
              Animated.update newTime animation
            }
            Cmd.none

    Message.Move dir ->
      let
        newLocation =
          Offset.add (Offset.fromDirection dir) level.coreLocation
        levelBox =
          { min =
            { x =
              0
            , y =
              0
            }
          , max =
            level.bound
          }
      in
        if
          -- TODO this check will need to be completely rewritten
          -- Box.within levelBox newLocation
          Box.within levelBox (Position.translate newLocation { x=0, y=0 })
        then
          Time.now
            |> Task.perform (Time.posixToMillis >> Message.AddPlayerAnimation dir)
            |>
              Update.InternalChange
                { level =
                  { level
                  | coreLocation =
                    newLocation
                  }
                , playerAnimation =
                  playerAnimation
                }
        else
          Update.NoChange

    Message.AddPlayerAnimation dir currentTime ->
      let
        offset : Offset Float
        offset =
          dir
            |> Offset.fromDirection
            |> Offset.scale -1
            |> Offset.toFloat
      in
        Update.InternalChange
          { level =
            level
          , playerAnimation =
            Just
              { currentTime =
                currentTime
              , duration =
                50
              , endingTime =
                50 + currentTime
              , dx =
                offset.dx
              , dy =
                offset.dy
              }
          }
          Cmd.none
