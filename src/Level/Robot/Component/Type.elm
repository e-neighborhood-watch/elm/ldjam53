module Level.Robot.Component.Type
  exposing
    ( Component (..)
    )


type Component
  = Core
  | Treads TreadDirection Int
  | Shield
  | Drill Dir
  | Cargo -- cargo should also take a Package as a parameter, but a Package can be a Component, and that gets recursive
  | Sensor Int
  | Memory Int
  | Scanner Int
