module LevelSelect.Model.Option.Type
  exposing
    ( Option
    , complete
    )


import Level.Status.Type
  as Status
  exposing
    ( WithStatus
    )
import Level.Type
  as Level
  exposing
    ( Level
    )


type alias Option =
  WithStatus Level


complete : Option -> Option
complete opt =
  opt
  |> Level.get
  |> Status.add Status.Complete
