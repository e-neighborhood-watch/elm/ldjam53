module LevelSelect.Update
  exposing
    ( update
    )


import Level.Type
  as Level
import LevelSelect.Message.Type
  exposing
    ( Message (..)
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )


import LevelSelect.Update.Result.Type
  as Update


update : Message -> Model -> Update.Result
update msg { selected, previous, remaining } =
  case
    msg
  of
    Select ->
      Update.Selected (Level.get selected)

    Previous ->
      case
        previous
      of
        [] ->
          Update.NoChange

        newSelected :: newPrevious ->
          Update.Updated
            { selected =
              newSelected
            , previous =
              newPrevious
            , remaining =
              selected :: remaining
            }

    Next ->
      case
        remaining
      of
        [] ->
          Update.NoChange

        newSelected :: newRemaining ->
          Update.Updated
            { selected =
              newSelected
            , previous =
              selected :: previous
            , remaining =
              newRemaining
            }
