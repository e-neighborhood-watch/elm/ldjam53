module LevelSelect.Init
  exposing
    ( init
    )


import Level.Status.Type
  as Status
import Level.Type
  exposing
    ( Level
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )
import LevelSelect.Model.Option.Type
  exposing
    ( Option
    )


initLevel : Level -> Option
initLevel =
  Status.add Status.Incomplete


init : Level -> List Level -> Model
init current remaining =
  { selected =
    initLevel current
  , previous =
    []
  , remaining =
    List.map initLevel remaining
  }
