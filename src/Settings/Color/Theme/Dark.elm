module Settings.Color.Theme.Dark
  exposing
    ( toHex
    )


import Data.Color.Theme.Type
  as Color
  exposing
    ( ThemeColor
    )


toHex : ThemeColor -> String
toHex color =
  case
    color
  of
    Color.Background ->
      "#424242"
    Color.Foreground ->
      "#888888"
