module Settings.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
import Svg.Styled.Events
  as Svg
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Data.Text.Type
  as Text
import Document.Type
  exposing
    ( Document
    )
import Settings.Color.Theme
  as Theme
import Settings.Color.Theme.Type
  as Theme
import Settings.Font.Size.Type
  as FontSize
import Settings.Language
  as Language
import Settings.Message.Type
  as Message
  exposing
    ( Message
    )
import Settings.Model.Type
  exposing
    ( Model
    )


view : Model -> Document Message
view model =
  let
    rectBox =
      { min =
        { x =
          -0.3
        , y =
          -0.3
        }
      , max =
        { x =
          7.3
        , y =
          7.3
        }
      }
    viewBox =
      { min =
        { x =
          -0.6
        , y =
          -0.6
        }
      , max =
        { x =
          8.2
        , y =
          8.2
        }
      }
  in
    { title =
      Language.fromText model.settings.language Text.SettingsTitle
    , viewBox =
      viewBox
    , body =
      [ Svg.rect
        [ SvgAttr.x (rectBox.min.x |> String.fromFloat)
        , SvgAttr.y (rectBox.min.y |> String.fromFloat)
        , SvgAttr.fill (Theme.toHex model.settings.theme Color.Background)
        , SvgAttr.width (rectBox.max.x - rectBox.min.x |> String.fromFloat)
        , SvgAttr.height (rectBox.max.y - rectBox.min.y |> String.fromFloat)
        , SvgAttr.stroke (Theme.toHex model.settings.theme Color.Foreground)
        , SvgAttr.strokeWidth "0.1"
        ]
        [
        ]
      , Svg.use
        [ SvgAttr.xlinkHref "#SettingsWheel"
        , SvgAttr.x (4.2 |> String.fromFloat)
        , SvgAttr.y (4.2 |> String.fromFloat)
        , SvgAttr.fill (Theme.toHex model.settings.theme Color.Foreground)
        , Svg.onClick (Message.SetTheme Theme.Light)
        ]
        [
        ]
      , Svg.use
        [ SvgAttr.xlinkHref "#SettingsWheel"
        , SvgAttr.x (3.2 |> String.fromFloat)
        , SvgAttr.y (4.2 |> String.fromFloat)
        , SvgAttr.fill (Theme.toHex model.settings.theme Color.Foreground)
        , Svg.onClick (Message.SetTheme Theme.Dark)
        ]
        [
        ]
      , Svg.use
        [ SvgAttr.xlinkHref "#SettingsWheel"
        , SvgAttr.x (2.2 |> String.fromFloat)
        , SvgAttr.y (2.2 |> String.fromFloat)
        , SvgAttr.fill (Theme.toHex model.settings.theme Color.Foreground)
        , Svg.onClick (Message.SetFontSize FontSize.Small)
        ]
        [
        ]
      , Svg.use
        [ SvgAttr.xlinkHref "#SettingsWheel"
        , SvgAttr.x (3.2 |> String.fromFloat)
        , SvgAttr.y (2.2 |> String.fromFloat)
        , SvgAttr.fill (Theme.toHex model.settings.theme Color.Foreground)
        , Svg.onClick (Message.SetFontSize FontSize.Medium)
        ]
        [
        ]
      , Svg.use
        [ SvgAttr.xlinkHref "#SettingsWheel"
        , SvgAttr.x (4.2 |> String.fromFloat)
        , SvgAttr.y (2.2 |> String.fromFloat)
        , SvgAttr.fill (Theme.toHex model.settings.theme Color.Foreground)
        , Svg.onClick (Message.SetFontSize FontSize.Large)
        ]
        [
        ]
      , Svg.use
        [ SvgAttr.xlinkHref "#AcceptIcon"
        , SvgAttr.x (rectBox.max.x - 1.8 |> String.fromFloat)
        , SvgAttr.y (rectBox.max.y - 0.8 |> String.fromFloat)
        , SvgAttr.stroke (Theme.toHex model.settings.theme Color.Foreground)
        , Svg.onClick Message.SaveAndExit
        ]
        [
        ]
      , Svg.use
        [ SvgAttr.xlinkHref "#CancelIcon"
        , SvgAttr.x (rectBox.max.x - 0.8 |> String.fromFloat)
        , SvgAttr.y (rectBox.max.y - 0.8 |> String.fromFloat)
        , SvgAttr.stroke (Theme.toHex model.settings.theme Color.Foreground)
        , Svg.onClick Message.Cancel
        ]
        [
        ]
      ]
    }
