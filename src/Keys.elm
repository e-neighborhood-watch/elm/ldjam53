module Keys
  exposing
    ( makeKeyDecoder
    )


import Dict
  exposing
    ( Dict
    )
import Json.Decode as Json


makeKeyDecoder : Dict String m -> Json.Decoder m
makeKeyDecoder keyLookup =
  Json.field "key" Json.string
    |>
      Json.andThen
        ( \ key ->
            case
              Dict.get key keyLookup
            of
              Just action ->
                Json.succeed action
              Nothing ->
                Json.fail "unrecognized key"
        )
