# Elm Game Template

We have at this point done a number of game jams in Elm, and we've noticed that the first few steps of developing them are virtually the same.
To avoid having to waste time and brainpower reinventing the wheel every single game jam, we are creating this template to serve as a starting point.
While we do plan to mostly use this for game jams, we expect that we will also use it to aid in regular game development.

## Instructions

To use this template you can create a fork.
You will need to run

```shell
git submodule init
git submodule update
```

when you first start with the project.
After that it should work with the elm reactor

```shell
elm reactor
```

To make the game you just start editting from there.

## Upload

To make a zip file for upload to itch.io you will need all the proper assets in your zip.
Run the following commands.

```shell
elm make --optimize src/Main.elm --output src/index.html
cd src/
zip -r game.zip index.html Data/Assets/
```

## Plans

* Audio
* Basic controls
* Simple placeholder graphics
* Animations

